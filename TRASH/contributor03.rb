#!/usr/bin/env ruby
require 'bunny'
require 'yaml'
require 'json'
#require 'bson-ruby'

MY_PREFIX = "tests"


begin

  # conn = Bunny.new
  # conn = Bunny.new host: '127.0.0.1', port: 5672
  conn = Bunny.new host: '127.0.0.1', port: 5600, user: 'poc_api', pass: '8I951vgi'
  conn.start

  channel = conn.create_channel
  @nba = channel.topic(MY_PREFIX)

rescue Bunny::TCPConnectionFailedForAllHosts
  puts "FAILED: cannot connect to RabbitMQ hosts"
  exit 1

end


def shout suffix, headers = {}, payload = {}

  # Add timestamp
  headers[:pushed_at] = DateTime.now.iso8601 if headers.is_a? Hash

  # Prepare key and data
  routing_key = "#{MY_PREFIX}.#{suffix.join('.')}"
  # payload = data

  # Announce on stdout
  puts
  puts "################# #{routing_key}"
  puts headers.inspect
  # Publish

  @nba.publish(payload.to_json,
    routing_key: routing_key,
    headers: headers,
    app_id: "contributor",
    )
end


loop do
  shout [:event1, :yahoo], {great: true}, {payload1: 1, payload1: 2}
  sleep 1
  shout [:event1, :splash], {spinning: 12, great: false}
  sleep 1
  shout [:event2, :yihaa], {great: -5}
  sleep 1
end

conn.close



