class ReportTable

  def initialize jobs, title
    @jobs = jobs
    @title = title
    puts "INITIALIZING \t#{@title}"
    @rows = build_rows
  end

  def table
    table = Terminal::Table.new
    table.title = @title

    # Add headers
    table.headings = @headers

    # Add rows
    table.rows = @rows

    # Return table
    table
  end

  def sheet spreadsheet
    # Build Excel sheet
    sheet = spreadsheet.create_worksheet :name => @title

    # Add headers
    sheet.row(0).replace @headers

    # Start feeding at line 2
    idx = 2
    @rows.each do |row|
      sheet.row(idx).replace row
      idx += 1
    end

    # Return spreadsheet
    spreadsheet
  end

  def ftime time
    time.strftime("%d/%m %H:%M:%S") if time.is_a? Time
  end

  def distance from, to
    return nil if from.nil? || to.nil?
    number (to - from)
  end

  def percent from, to, versus
    return nil if from.nil? || to.nil? || versus.nil? || versus.zero?
    number (100*(to - from)/versus.to_f)
  end

private

  def number number, dec: 1
    number.to_f.round(dec)
  end

end
