#!/usr/bin/env ruby
require 'yaml'
require 'json'
require 'hashie'
require 'bunny'
require 'terminal-table'
require 'rest_client'


QUEUE_PREFIX = "push"
QUEUE_HOST = `hostname`.to_s.chomp


def payload_extract payload, fields = []
    new_payload = payload.force_encoding('UTF-8')
    parsed = JSON.parse new_payload

  rescue Encoding::UndefinedConversionError => e
    puts "\t JSON PARSE ERROR: #{e.inspect}"
    return {}

  else
    return parsed
end

def propagate url, body
  # Nothing more to do if no relay
  return if url.nil? || url.empty?

  # Push message to URL
  puts "> POST #{url}"
  response = RestClient.post url.to_s, body, :content_type => :json
  puts "< #{response.body}"

  rescue Exception => e
    puts "> FAILED: #{e.message}"

end

def handle_message rule_name, rule, delivery_info, metadata, payload
  # Prepare data
  msg_topic = delivery_info.exchange
  msg_rkey = delivery_info.routing_key.force_encoding('UTF-8')
  msg_headers = metadata.headers
  msg_appid = metadata.app_id

  # Propagated message payload
  if fields = rule[:payload_extract]
    data = payload_extract(payload, fields)
    data_source = "extract #{fields.inspect} #{data.keys.count}k"

  elsif msg_headers.is_a? Hash
    data = msg_headers
    data_source = "headers #{data.keys.count}k"

  else
    data = :data_not_a_hash
    data_source = "none"
  end

  # Announce match
  puts "="*160
  puts sprintf "%-20s | %-10s | %-10s | %-20s | %-40s",
    rule_name, msg_topic, msg_appid, data_source, msg_rkey

  # Build notification payload
  body = {
    # received: msg_topic,
    exchange: msg_topic,
    rkey: msg_rkey,
    headers: msg_headers,
    #payload: payload,
    # _payload: payload,
    # _headers: msg_headers,
    data: data,
    metadata: metadata.to_hash,
    #delivery_info: delivery_info.to_hash
    }
  pretty_body = JSON.pretty_generate(body)

  # Dump body data
  puts "="*160
  puts pretty_body

  # Propagate data if needed
  propagate rule[:relay], pretty_body

  # We're done
  puts
end


# Load config file
config_name = ARGV[0]
# puts ARGV.to_yaml
abort "USAGE: #{$0} profile" if config_name.to_s.empty?

config_file = File.join(File.dirname(File.expand_path(__FILE__)), "#{config_name}.yml")
abort "ABORT: missgin config file [#{config_file}]" unless File.exist? config_file

config = YAML::load_file(config_file)
abort "ABORT: invalid config file [#{config_file}]" unless config.is_a? Hash

# Dump configuration
Hashie.symbolize_keys! config
puts config.to_yaml


# Init ASCII table
config_table = Terminal::Table.new
config_table.title = "Message propagation rules"
config_table.headings = ["queue binding", "topic", "route", "relay", "title"]
config_table.align_column(5, :right)


# Start connexion to RabbitMQ
busconf = config[:bus]
abort "ABORT: bus host/port not found" unless busconf.is_a? Hash
begin
  puts "connecting to #{busconf[:host]} port #{busconf[:port]}"
  conn = Bunny.new host: (busconf[:host].to_s || "localhost").to_s,
    port: (busconf[:port] || 5672).to_i,
    user: (busconf[:user] || "guest").to_s,
    pass: (busconf[:pass] || "guest").to_s,
    heartbeat: :server
  conn.start
  channel = conn.create_channel
rescue Bunny::TCPConnectionFailedForAllHosts, Bunny::AuthenticationFailureError, AMQ::Protocol::EmptyResponseError  => e
  abort "ABORT: error connecting to RabbitMQ (#{e.class})"
rescue Exception => e
  abort "ABORT: unknow connection error (#{e.inspect})"
end


# Bind every topic
abort "ABORT: config file missing [rules] section" unless config[:rules].is_a? Hash
topics = {}
config[:rules].each do |rule_name, rule|
  # Extract information
  catch_topic = rule[:topic].to_s
  catch_routes = rule[:routes].to_s.split(' ')

  if catch_topic.empty? || catch_routes.empty?
    abort "rule [#{rule_name}] is invalid: missing topic / routes"
  end

  # Build / attach to queue
  rule_queue_name = "#{QUEUE_PREFIX}/#{QUEUE_HOST}/#{rule_name}"

  begin
    # Bind to this topic if not already done
    exchange = topics[catch_topic] ||= channel.topic(catch_topic, durable: true)

    # Pour this into a queue
    queue = channel.queue(rule_queue_name, auto_delete: true)

    # Bind to these events on each route
    catch_routes.each do |route|
      # Bind with this routing key
      queue.bind exchange, routing_key: route
      puts "BIND \t[#{rule_queue_name}] to [#{catch_topic}] / [#{route}]"

      # Add row to table
      config_table.add_row [rule_queue_name, catch_topic, route, rule[:relay].to_s, rule[:title].to_s ]
    end

  rescue Bunny::PreconditionFailed => e
    conn.create_channel.queue_delete(rule_queue_name)
    # puts e.backtrace.inspect
    abort "ABORT: cannot bind [#{catch_topic}] code(#{e.channel_close.reply_code}) message(#{e.channel_close.reply_text})"

  rescue Exception => e
    abort "ABORT: unhandled (#{e.inspect})"

  ensure
  end


  # Subscribe
  queue.subscribe(block: false) do |delivery_info, metadata, payload|
    handle_message rule_name, rule, delivery_info, metadata, payload
  end
end


# Display config and susbcribe to queue
puts config_table

# Endless loop
begin
  loop do
    sleep 1
  end
rescue AMQ::Protocol::EmptyResponseError
  abort "ABORT: AMQ::Protocol::EmptyResponseError (#{e.inspect})"
rescue Interrupt => _
  channel.close
  conn.close
end

puts
puts "ENDED"
