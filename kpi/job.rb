class Job

  attr_accessor :job_id
  attr_accessor :plurimedia
  attr_accessor :jobtype

  def initialize params
    @params = params
    analyze
  end

  def dump
    puts @params.inspect
  end

  def has_id?
    @params[:id]? true : false
  end

  def analyze
    # Extract attributes
    @job_id       = @params["job_id"].to_i
    @plurimedia   = @params["plurimedia"]
    @jobtype      = @params["type"]

    # Extract root tamps
    jstamp :started_at
    jstamp :top_start
    jstamp :top_end

    # Extract expected
    texpect :prepare
    texpect :prepare_ingest
    texpect :ohe_decoupe
    texpect :ohe_sprites
    texpect :transfo_web
    texpect :transfo_fai

    # Extract stamps: OHE
    tstamp :ohe_decoupe, :done
    tstamp :ohe_decoupe, :started
    tstamp :ohe_decoupe, :complete

    # Extract stamps: Ingest
    tstamp :prepare_ingest, :done

    # Extract stamps: Arkena
    tstamp :transfer_web, :done
    tstamp :transfer_web, :started
    tstamp :transfer_web, :complete

    tstamp :transfo_web, :done
    tstamp :transfo_web, :started
    tstamp :transfo_web, :video
    tstamp :transfo_web, :transfer
    tstamp :transfo_web, :complete

    # Extract stamps: Pixagility
    tstamp :transfer_fai, :done
    tstamp :transfer_fai, :started
    tstamp :transfer_fai, :complete

    tstamp :transfo_fai, :done
    tstamp :transfo_fai, :started
    tstamp :transfo_fai, :video
    tstamp :transfo_fai, :complete

    # Extract stamps: Ingest
    tstamp :transfo_ingest, :done
    tstamp :transfo_ingest, :started
    tstamp :transfo_ingest, :complete

    tstamp :transfo_fai, :done
    tstamp :transfo_fai, :started
    tstamp :transfo_fai, :video
    tstamp :transfo_fai, :complete

    # Extract stamps: other
    tstamp :activation_web, :done
    tstamp :activation_ingest, :done
  end

  def random?
    rand(2)>=1
  end

  # def no_decoupe_expected?
  #   !@expect_ohe_decoupe
  # end

  def decoupe_expected?
    @expect_ohe_decoupe
  end
  def decoupe_complete?
    @ohe_decoupe_complete
  end

  def web_expected?
    @expect_transfo_web
  end
  def web_complete?
    @transfo_web_complete
  end
  def web_activated?
    @activation_web_done
  end

  def fai_expected?
    @expect_transfo_fai
  end
  def fai_complete?
    @transfo_fai_complete
  end

  def ingest_expected?
    @expect_prepare_ingest
  end
  def ingest_activated?
    @activation_ingest_done
  end

  # def decoupe_expected_and_not_complete?
  #   @expect_ohe_decoupe && !@ohe_decoupe_complete
  # end

  # def prepare_ingest_expected_and_done?
  #   @expect_prepare_ingest && @prepare_ingest_done
  # end

  # def transfo_ingest_expected_and_done?
  #   @expect_transfo_ingest && @transfo_ingest_complete
  #   #{expected_transfo_fai: @expect_transfo_fai, transfo_fai_complete: @transfo_fai_complete}
  # end

  def aborted_early?

  end


private

  def mktime blabla
    Time.parse blabla rescue nil
  end

  def jstamp key
    declare key, mktime(@params[key.to_s])
  end

  def tstamp task, signal
    declare "#{task}_#{signal}", mktime(@params["tasks"][task.to_s][signal.to_s]) if @params["tasks"][task.to_s].is_a? Hash
  end

  def texpect task
    not_expected = @params["tasks"][task.to_s].nil? || @params["tasks"][task.to_s]["expect"] == false
    declare "expect_#{task}", !not_expected
  end

  # def tbool task, signal
  #   declare "#{task}_#{signal}", @params["tasks"][task.to_s][signal.to_s]? true : false)
  # end

  # Declare accessor
  def declare name, value
    instance_variable_set "@#{name}", value
    self.class.__send__(:attr_reader, name.to_s)
  end

end
