class ReportJobs < ReportTable

  def initialize jobs, title
    @headers = [
      "job_id", "plurimedia", "top_start", "top_end", "duration",

      # Decoupe
      "ohe.do>st", "ohe.st>co", "ohe %",

      # Arkena
      "tx_web.do>st", "tx_web.st>co", "tx_web %",
      "to_web.do>st", "to_web.st>vi", "st>vi %",
      "to_web.vi>act_web.do",

      # Pixagility
      "tx_fai.do>st", "tx_fai.st>en", "tx_fai %",
      "to_fai.do>st", "to_fai.st>vi", "st>vi %"
      ]

    super
  end

  def build_rows
    @jobs.collect do |job|
      build_row job
    end
  end

  def build_row job
    out = []

    # Computations
    if (job.top_end && job.top_start)
      duration = (job.top_end - job.top_start)
    end

    # First cells
    out << job.job_id
    out << job.plurimedia
    out << job.top_start
    out << job.top_end
    out << duration

    # Decoupe
    out << distance(  job.ohe_decoupe_done,     job.ohe_decoupe_started     )
    out << distance(  job.ohe_decoupe_started,  job.ohe_decoupe_complete    )
    out << percent(   job.ohe_decoupe_started,  job.ohe_decoupe_complete,   duration)

    # Arkena
    out << distance(  job.transfer_web_done,    job.transfer_web_started    )
    out << distance(  job.transfer_web_started, job.transfer_web_complete   )
    out << percent(   job.transfer_web_started, job.transfer_web_complete,  duration)

    out << distance(  job.transfo_web_done,     job.transfo_web_started     )
    out << distance(  job.transfo_web_started,  job.transfo_web_video       )
    out << percent(   job.transfo_web_started,  job.transfo_web_video,      duration)

    out << distance(  job.transfo_web_video,    job.activation_web_done     )

    # Pixagility
    out << distance(  job.transfer_fai_done,    job.transfer_fai_started    )
    out << distance(  job.transfer_fai_started, job.transfer_fai_complete   )
    out << percent(   job.transfer_fai_started, job.transfer_fai_complete,  duration)

    out << distance(  job.transfo_fai_done,     job.transfo_fai_started     )
    out << distance(  job.transfo_fai_started,  job.transfo_fai_video       )
    out << percent(   job.transfo_fai_started,  job.transfo_fai_video,      duration)

    out
  end

end
