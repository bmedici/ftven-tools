#!/usr/bin/env ruby
require 'bunny'
require 'terminal-table'
require 'rest_client'
require 'yaml'


CONFIG_FILE = File.join(File.dirname(File.expand_path(__FILE__)), 'config.yml')
MY_QUEUE = "proxy02"


def propagate rule_name, rule_relay, delivery_info, metadata, payload
  # Announce
  puts
  puts "############ MATCHED [#{rule_name}]"

  # Build notification payload
  propagated = {
    exchange: delivery_info.exchange,
    rkey: delivery_info.routing_key,
    headers: metadata.headers,
    payload: payload,
  }
  puts propagated.to_yaml

  # Dump message contents
  # puts "delivery_info / exchange: #{delivery_info.exchange}"
  # puts "delivery_info / routing_key: #{delivery_info.routing_key}"
  # puts "metadata / content_type: #{metadata.content_type}"
  # puts "metadata / priority: #{metadata.priority}"
  # puts "metadata / delivery_mode: #{metadata.delivery_mode}"
  # puts "metadata / headers: #{metadata.headers.inspect}"
  # puts "payload: #{payload}"
  # puts "propagated: #{propagated.inspect}"

  # Push message to URL
  if (rule_relay)
    puts "POSTING TO: #{rule_relay}"
    response = RestClient.post rule_relay, propagated.to_json, :content_type => :json
    puts "POST REPLY: #{response.body}"
  else
    puts "SKIPPING: no valid [relay] provided"
  end
  puts
end


# Load config file
abort "invalid config file" unless File.exist? CONFIG_FILE
config = YAML::load_file(CONFIG_FILE)
abort "invalid config file" unless config.is_a? Hash
puts config.to_yaml


# Init ASCII table
config_table = Terminal::Table.new
config_table.title = "Message propagation rules"
config_table.headings = ["rule", "topic", "route", "relay", "title"]
config_table.align_column(5, :right)


# Start connexion to RabbitMQ
conn = Bunny.new
conn.start
channel = conn.create_channel


# Bind every topic
abort "config file missing [rules] section" unless config["rules"].is_a? Hash
topics = {}


config["rules"].each do |rule_name, rule_params|
  # Extract information
  catch_title = rule_params["title"].to_s
  catch_topic = rule_params["topic"].to_s
  catch_relay = rule_params["relay"].to_s
  catch_routes = rule_params["routes"].to_s.split(' ')
  # puts "#{rule_name}: routes=#{catch_routes} (#{rule_params[:routes]})"

  if catch_topic.empty? || catch_routes.empty? || catch_relay.empty?
    abort "rule [#{rule_name}] is invalid: missing topic, routes or relay attributes"
  end

  # Bind to this topic if not already done
  topic = topics[catch_topic] ||= channel.topic(catch_topic)

  # Subscribe to these events on each route
  catch_routes.each do |route|

    # Subscribe
    channel.queue(MY_QUEUE, :auto_delete => true).bind(topic, :routing_key => route).subscribe do |delivery_info, metadata, payload|
      propagate(rule_name, catch_relay, delivery_info, metadata, payload)
    end

    # Add row to table
    config_table.add_row [
      rule_name.to_s, catch_topic, route, catch_relay, catch_title
    ]

  end

end

# Display config

puts config_table

sleep 60

conn.close



