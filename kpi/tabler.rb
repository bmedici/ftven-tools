require 'terminal-table'

class Tabler
  attr_accessor :title
  attr_accessor :headers

  def initialize
    @rows = []
    @headers = {}
  end

  def row values
    @rows << values
  end

  def to_csv
  end

  def row_to_array row
    @headers
  end

  def to_terminal
    # Build plurimedia list
    # puts @title
    table = Terminal::Table.new
    table.title = @title
    table.headings = @headers.values
    table.rows = @rows.collect{ |row| row_to_array row }
    puts table
  end

end
