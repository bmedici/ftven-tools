#!/usr/bin/env ruby
require 'bunny'


conn = Bunny.new
conn.start

channel = conn.create_channel
nba = channel.topic("tests")

loop do
  print "F"
  nba.publish("France !",
    routing_key: "europe.france",
    headers: {
      :participants => 11,
      }
    )
  sleep 1
  print "I"
  nba.publish("Italy !",
    routing_key: "europe.italy"
    )
  sleep 1
end

conn.close



