#!/usr/bin/env ruby
require_relative 'common'

SHOUT_PREFIX = "tests"




# Load config file
config = read_config

# Start connexion to RabbitMQ and create channel
conn = connect config[:bus]
channel = conn.create_channel


begin
  # Clear any previous queue
  #channel.queue_delete(SHOUT_PREFIX)

  # Recreate it
  exchange = channel.topic(SHOUT_PREFIX, :auto_delete => true)

  loop do
    shout exchange, [:event1, :yahoo], {great: true}, {payload1: 1, payload2: 2}
    sleep 1
    shout exchange, [:event1, :splash], {spinning: 12, great: false}
    sleep 1
    shout exchange, [:event2, :yihaa], {great: -5}
    sleep 1
  end

rescue Bunny::TCPConnectionFailedForAllHosts => e
  puts "FAILED: cannot connect to RabbitMQ hosts (#{e.inspect})"
  exit 1
rescue Bunny::PreconditionFailed => e
  puts "FAILED: precondition failed (#{e.inspect})"
  exit 1

rescue Exception => e
  puts "UNEXPECTED: #{e.inspect}"
  exit 1

end

conn.close



