class ReportStatus < ReportTable

  def initialize jobs, title
    @headers = [
      "id", "count",
      "decoupe?", "decoupe ok",
      "web?", "web ok", "web act",
      "fai?", "fai ok",
      "ingest?", "ingest act"
      ]

    super
  end

  def build_rows
    # @jobs.group_by{ |job| job.started_at.strftime('%Y-%m-%d-%H') }.collect do |serie, jobs|
    @jobs.group_by{ |job| "#{job.started_at.strftime('%Y-S%V')}-#{job.jobtype}" }.collect do |serie, jobs|
      build_row serie, jobs
    end
  end

  def build_row serie, jobs
    out = []

    # Computations
    out << serie
    out << jobs.count

    out << totalize(jobs, :decoupe_expected)
    out << totalize(jobs, :decoupe_complete)

    out << totalize(jobs, :web_expected)
    out << totalize(jobs, :web_complete)
    out << totalize(jobs, :web_activated)

    out << totalize(jobs, :fai_expected)
    out << totalize(jobs, :fai_complete)

    out << totalize(jobs, :ingest_expected)
    out << totalize(jobs, :ingest_activated)

    out
  end

  def totalize jobs, status
    return jobs.select{ |job| job.send("#{status.to_s}?")}.count

    out = {}
    jobs.each do |job|
      out[job.job_id] = job.send("#{status.to_s}?")
    end
    return out.inspect
  end

end
