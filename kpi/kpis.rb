#!/usr/bin/env ruby

require 'spreadsheet'
require 'terminal-table'
require 'mail'
require 'tempfile'
require 'rest-client'

require_relative 'helpers'
require_relative 'job'

require_relative 'report_table'
require_relative 'report_jobs'
require_relative 'report_pluri'
require_relative 'report_status'


API_MAXITEMS  = 200

API_URL       = "http://webservices.francetelevisions.fr/replay/jobs?limit=#{API_MAXITEMS}&sort=r.jobId&direction=desc"
SEND_SUBJECT  = "[Replay FTVEN] Rapport KPIs"
SEND_FROM     = "Bruno <bruno.medici.ext@francetv.fr>"
# SEND_TO       = "Bruno <bruno.medici.ext@francetv.fr>; Bruno MBC <bmedici@bmconseil.com>"
SEND_TO       = "Bruno MBC <bmedici@bmconseil.com>"
# SEND_TO       = "Bruno <bruno.medici.ext@francetv.fr>; Hassan <hassan.samhat.ext@francetv.fr>"


LOCAL_JOBS = File.join(File.dirname(File.expand_path(__FILE__)), 'sample100.json')
# LOCAL_JOBS = ""

EXCEL_PATH = Tempfile.new('replay_kpi')
# EXCEL_PATH = File.open('/Users/bruno/replay_kpi.xls', 'w+')


####################################################################################################
## Prepare data
####################################################################################################

# Fetch jobs and build collection
json = if File.exist? LOCAL_JOBS
  json_read LOCAL_JOBS
else
  json_fetch API_URL
end
jobs = get_jobs json

# puts "Jobs have been prepared"
# puts jobs.select{|job| job.job_id == 482251}.to_yaml
# puts jobs.select{|job| job.job_id == 482251}.methods.to_yaml

# puts jobs.to_yaml
# puts jobs.first.to_yaml
# exit 0



####################################################################################################
## Build reports
####################################################################################################

report_status = ReportStatus.new(jobs, "Report by status")

report_pm = ReportPluri.new(jobs, "Report by Plurimedia ID")

report_jobs = ReportJobs.new(jobs, "Linear jobs list")




####################################################################################################
## Output: screen
####################################################################################################

# Display output
ascii = [report_status, report_pm, report_jobs].collect(&:table).join("\n\n\n\n")
# ascii = [report2].collect(&:table).join("\n\n\n\n")
puts ascii

# exit 0

#puts ascii


####################################################################################################
## Output: email
####################################################################################################
puts "EMAILING \t[#{SEND_FROM}] > [#{SEND_TO}]"

# Create a new Workbook
spreadsheet = Spreadsheet::Workbook.new
report_status.sheet spreadsheet
report_pm.sheet spreadsheet
report_jobs.sheet spreadsheet

# Compile spreadsheet
spreadsheet.write(EXCEL_PATH)

#exit 1

# Send email
timestamp = Time.now

subject = "#{SEND_SUBJECT} #{Time.now.strftime('%d/%m %H:%M:%S')}"
filename = "kpis_#{timestamp.strftime('%y%m%d_%H%M%S')}.xls"
filename = "kpis.xls"

# puts filename
Mail.deliver do
  delivery_method :sendmail
  from      SEND_FROM
  to        SEND_TO
  subject   subject
  body      ascii
  add_file  filename: filename, content: EXCEL_PATH.read
end




