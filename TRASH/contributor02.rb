#!/usr/bin/env ruby
require 'bunny'
require 'yaml'

MY_PREFIX = "tests"


begin

  conn = Bunny.new
  conn.start

  channel = conn.create_channel
  @nba = channel.topic(MY_PREFIX)

rescue Bunny::TCPConnectionFailedForAllHosts
  puts "FAILED: cannot connect to RabbitMQ hosts"
  exit 1

end


def shout suffix, headers = {}, payload = ""

  # Add timestamp
  headers[:pushed_at] = DateTime.now.iso8601 if headers.is_a? Hash

  # Prepare key
  routing_key = "#{MY_PREFIX}.#{suffix.join('.')}"

  # Announce on stdout
  puts
  puts "################# #{routing_key}"
  puts headers.inspect
  # Publish

  @nba.publish(payload.to_s,
    routing_key: routing_key,
    headers: headers
    )
end


loop do
  shout [:event1, :yahoo], {great: true}
  sleep 1
  shout [:event1, :splash], {spinning: 12, great: false}
  sleep 1
  shout [:event2, :yihaa], {great: -5}
  sleep 1
end

conn.close



