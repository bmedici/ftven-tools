#!/usr/bin/env ruby
require "rubygems"

require 'terminal-table'
require 'rest_client'
require 'yaml'
require 'hashie'

require 'eventmachine'
require 'amqp'


CONFIG_FILE = File.join(File.dirname(File.expand_path(__FILE__)), 'config.yml')
QUEUE_PREFIX = "push-proxy"
QUEUE_VERSION = "v03"


def propagate rule_name, rule_relay, delivery_info, metadata, msg_payload
  # Announce
  msg_topic = delivery_info.exchange
  msg_rkey = delivery_info.routing_key
  msg_headers = metadata.headers
  msg_appid = metadata.app_id

  # puts "############ MATCHED [#{rule_name}]"
  printf "# %-12s | %-8s | %-25s | %-12s | %-90s | ",
    rule_name, msg_topic, msg_rkey, msg_appid, msg_headers.inspect


  # Build notification payload
  propagated = {
    exchange: msg_topic,
    rkey: msg_rkey,
    headers: msg_headers,
    payload: msg_payload,
    }

  # Dump message contents
  # puts propagated.to_yaml
  # puts "payload: #{msg_payload.inspect}"
  # puts "metadata: #{metadata.inspect}"
  # puts "metadata / content_type: #{metadata.content_type}"
  # puts "metadata / delivery_mode: #{metadata.delivery_mode}"
  # puts "metadata / headers: #{metadata.headers.inspect}"

  # Push message to URL
  if (rule_relay)
    # print "   posting to [#{rule_relay}]: "
    print "POST: "
    response = RestClient.post rule_relay, propagated.to_json, :content_type => :json
    print response.body
    puts
  else
    #puts "   nothing to do"
    puts "NOTHING"
  end
  # puts
end


# Load config file
abort "ABORT: invalid config file" unless File.exist? CONFIG_FILE
config = YAML::load_file(CONFIG_FILE)
abort "ABORT: invalid config file" unless config.is_a? Hash
Hashie.symbolize_keys! config
puts config.to_yaml


# Init ASCII table
config_table = Terminal::Table.new
config_table.title = "Message propagation rules"
config_table.headings = ["queue binding", "topic", "route", "relay", "title"]
config_table.align_column(5, :right)






# Prepare connexion to RabbitMQ
busconf = config[:bus]
abort "ABORT: amqp server missing" if config[:server].to_s.empty?
abort "ABORT: config file missing [rules] section" unless config[:rules].is_a? Hash
exchanges = {}


# Start connexion to RabbitMQ
# busconf = config[:bus]
# abort "ABORT: bus host/port not found" unless busconf.is_a? Hash
# begin
#   puts "connecting to #{busconf[:host]} port #{busconf[:port]}"
#   conn = Bunny.new host: (busconf["host"].to_s || "localhost").to_s,
#     port: (busconf[:port] || 5672).to_i,
#     user: (busconf[:user] || "guest").to_s,
#     pass: (busconf[:pass] || "guest").to_s,
#     heartbeat: :server
#   conn.start
#   channel = conn.create_channel
# rescue Bunny::TCPConnectionFailedForAllHosts, Bunny::AuthenticationFailureError  => e
#   abort "ABORT: error connecting to RabbitMQ (#{e.class})"
# rescue Exception => e
#   abort "ABORT: unknow connection error (#{e.inspect})"
# end


# Bind every topic
AMQP.start(config[:server]) do |connection|
  channel  = AMQP::Channel.new(connection)

  config[:rules].each do |rule_name, rule_params|
    # Extract information
    catch_title = rule_params[:title].to_s
    catch_topic = rule_params[:topic].to_s
    catch_relay = rule_params[:relay].to_s
    catch_routes = rule_params[:routes].to_s.split(' ')
    #puts "CONFIG #{rule_name}: routes=#{catch_routes} (#{rule_params[:routes]}) #{rule_params.inspect}"

    if catch_topic.empty? || catch_routes.empty?
      abort "rule [#{rule_name}] is invalid: missing topic / routes"
    end

    # Build / attach to queue
    rule_queue_name = "#{QUEUE_PREFIX}/#{QUEUE_VERSION}/#{rule_name}"

    begin
      # Create a new channel
      #channel = conn.create_channel

      # Bind to this topic if not already done
      exchange = exchanges[catch_topic] ||= channel.topic(catch_topic)

      # Pour this into a queue
      # queue = channel.queue(rule_queue_name, auto_delete: true, exclusive: false, durable: true)
      puts "QUEUE \t[#{rule_queue_name}]"
      #{}" exists: #{conn.queue_exists?(rule_queue_name)}"

      # queue = channel.queue(rule_queue_name).delete
      queue = channel.queue(rule_queue_name, {auto_delete: false, durable: true})
      # , exclusive: false,
      #queue.declare(auto_delete: true, exclusive: false, durable: true, passive: true)
      # queue = channel.queue(rule_queue_name, auto_delete: true, exclusive: false, durable: true)

      # Bind to these events on each route
      catch_routes.each do |route|
        # Bind with this routing key
        queue.bind exchange, routing_key: route
        puts "BIND \t[#{rule_queue_name}]: [#{catch_topic}]/[#{route}]"

        # Add row to table
        config_table.add_row [rule_queue_name, catch_topic, route, catch_relay, catch_title ]
      end

    # rescue Bunny::PreconditionFailed => e
    #   conn.create_channel.queue_delete(rule_queue_name)
    #   abort "ABORT: cannot bind [#{catch_topic}] code(#{e.channel_close.reply_code}) message(#{e.channel_close.reply_text})"
    rescue Exception => e
      abort "ABORT: unhandled (#{e.inspect})"

    ensure
    end


    # Subscribe
    queue.subscribe(block: false) do |delivery_info, metadata, payload|
      propagate rule_name, catch_relay, delivery_info, metadata, payload
    end

  end

  # disconnect & exit after 2 seconds
  EventMachine.add_timer(10) do
    #exchange.delete
    connection.close { EventMachine.stop }
  end

end




# Display config and susbcribe to queue
puts config_table

# Endless loop
begin
  loop do
    sleep 1
  end
rescue AMQ::Protocol::EmptyResponseError
  abort "ABORT: AMQ::Protocol::EmptyResponseError (#{e.inspect})"
rescue Interrupt => _
  channel.close
  conn.close
end

puts
puts "ENDED"
