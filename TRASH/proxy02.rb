#!/usr/bin/env ruby
require 'bunny'
require 'terminal-table'



MY_QUEUE = "proxy02"
SUBSCRIBE_TO = [:tests, :pftop]


RULES = {
rule1: {
  title: "All tests messages",
  topic: "tests",
  routes: ["tests.#"],
  relay: "http://requestb.in/1k12us21"
  },
rule2: {
  title: "event1 and event2 test messages",
  topic: "tests",
  routes: ["tests.event1.#", "tests.event2.#"],
  relay: "http://requestb.in/1k12us21"
  },
rule3: {
  title: "PFTOP broadcasts",
  topic: "pftops",
  routes: ["broadcast.#"],
  relay: "http://requestb.in/1k12us21"
  },
}




conn = Bunny.new
conn.start

channel = conn.create_channel

topic_tests = channel.topic("tests")
topic_pftop = channel.topic("pftop")

def propagate rule, delivery_info, metadata, payload
puts
  puts "############ RULE: #{rule[:title]}"
#  puts "  delivery_info: #{delivery_info.inspect}"
  puts "  delivery_info"
  puts "    exchange: #{delivery_info.exchange}"
  puts "    routing_key: #{delivery_info.routing_key}"
  puts "  metadata"
  puts "    content_type: #{metadata.content_type}"
  puts "    priority: #{metadata.priority}"
  puts "    delivery_mode: #{metadata.delivery_mode}"
  puts "    headers: #{metadata.headers.inspect}"
  puts "  payload: #{payload}"

  # Push message to URL
  puts rule[:relay]

  rule[:title]
end


# Initialize things
# Build ASCII table
config_table = Terminal::Table.new
config_table.title = "Message propagation rules"
config_table.headings = ["rule", "topic", "route", "relay", "title"]
config_table.align_column(5, :right)

# Bind every topic
topics = {}
RULES.each do |rule_name, rule_params|
  # Extract information
  catch_title = rule_params[:topic]
  catch_topic = rule_params[:topic]
  catch_routes = rule_params[:routes]
  catch_relay = rule_params[:relay]
  catch_title = rule_params[:title]

  # Bind to this topic if not already done
  topics[catch_topic] ||= channel.topic(catch_topic)

  # Subscribe to these events on each route
  catch_routes.each do |route|

    # Subscribe
    channel.queue(MY_QUEUE, :auto_delete => true).bind(topic_tests, :routing_key => route).subscribe do |delivery_info, metadata, payload|
      propagate(rule_params, delivery_info, metadata, payload)
    end

    # Add row to table
    config_table.add_row [
      rule_name, catch_topic, route, catch_relay, catch_title
    ]

  end

end

# Display config

puts config_table

sleep 60

conn.close



