#!/usr/bin/env ruby
require 'bunny'


conn = Bunny.new
conn.start

channel = conn.create_channel
nba = channel.topic("tests")


channel.queue("france_consumer", :auto_delete => true).bind(nba, :routing_key => "europe.france").subscribe do |delivery_info, metadata, payload|
  puts
  puts "consumer1 received"
#  puts "  delivery_info: #{delivery_info.inspect}"
  puts "  delivery_info"
  puts "    exchange: #{delivery_info[:exchange]}"
  puts "    routing_key: #{delivery_info[:routing_key]}"
  puts "    exchange: #{delivery_info[:xx]}"
  puts "  metadata: #{metadata.inspect}"
  puts "  payload: #{payload}"
end

sleep 60

conn.close



