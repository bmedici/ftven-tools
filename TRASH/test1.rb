#!/usr/bin/env ruby

require 'bunny'

conn = Bunny.new
conn.start

ch = conn.create_channel
nba = ch.fanout("nba.scores")


ch.queue("joe", :auto_delete => true).bind(nba).subscribe do |delivery_info, metadata, payload|
  puts "#{payload} => joe"
end

ch.queue("aaron", :auto_delete => true).bind(nba).subscribe do |delivery_info, metadata, payload|
  puts "#{payload} => aaron"
end

ch.queue("bob", :auto_delete => true).bind(nba).subscribe do |delivery_info, metadata, payload|
  puts "#{payload} => bob"
end

nba.publish("nba/message1")
nba.publish("nba/message2")

sleep 2

conn.close



