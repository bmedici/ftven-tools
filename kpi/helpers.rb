require 'rest-client'
require 'json'
require 'time'

def get_jobs json
  parsed = JSON.parse(json) rescue nil
  if parsed.is_a? Enumerable
    parsed.collect { |job| Job.new(job) }
  else
    {}
  end
end

def json_fetch url
  puts "FETCHING \t#{API_URL}"
  response = RestClient.get url, :content_type => :json
  response.body
end

def json_read file
  puts "READING \t#{file}"
  File.read file
end

# def filter_jobs_aged 10
# end

def get_time job, key
  Time.parse job[key.to_s] rescue nil
end

def get_stamp job, task, signal
  Time.parse job["tasks"][task.to_s][signal.to_s] rescue nil
end
