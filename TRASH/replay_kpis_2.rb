#!/usr/bin/env ruby

#require 'rest_client'
require 'rest-client'
require 'json'
require 'terminal-table'
require 'time'

API_URL = "http://webservices.francetelevisions.fr/replay/jobs?limit=100&sort=r.jobId&direction=desc"


def fetch
  response = RestClient.get API_URL, :content_type => :json
  parsed = JSON.parse(response.body) rescue nil
  return parsed
end

def get_time job, key
  Time.parse job[key.to_s] rescue nil
end

def get_stamp job, task, signal
  Time.parse job["tasks"][task.to_s][signal.to_s] rescue nil
end

def get_distance from, to
  return nil if from.nil? || to.nil?
  (to - from).to_f.round(1)
end

def get_percent from, to, versus
  return nil if from.nil? || to.nil? || versus.zero?
  (100*(to - from)/versus.to_f).round(1)
end

def build_plurimedia jobs
  jobs.group_by{ |job| job["plurimedia"] }.collect do |plurimedia, jobs|
    out = []

    created_at_s = jobs.collect{ |job| get_time job, :started_at }

    ohe_decoupe_done_s = jobs.collect{ |job| get_stamp job, :ohe_decoupe, :done }.reject{ |t| t.nil? }
    min_ohe_decoupe_done = ohe_decoupe_done_s.min

    job_with_latest_created_at = jobs.sort_by{ |job| get_time job, :created_at}.last
    top_end_of_job_with_latest_created_at = get_time job_with_latest_created_at, :top_end

    activation_web_done_s = jobs.collect{ |job| get_stamp job, :activation_web, :done }.reject{ |t| t.nil? }
    max_activation_web_done = activation_web_done_s.max

    out << plurimedia
    out << jobs.count
    out << jobs.collect{ |job| job["job_id"] }.inspect
    out << created_at_s.min
    out << created_at_s.max

    out << min_ohe_decoupe_done
    out << top_end_of_job_with_latest_created_at
    out << get_distance(top_end_of_job_with_latest_created_at, min_ohe_decoupe_done)

    out << max_activation_web_done
    out << get_distance(min_ohe_decoupe_done, max_activation_web_done)

    out
  end
end

def table_jobs_row job
  out = []

  # Extract attributes
  top_start   = get_time job, :top_start
  top_end     = get_time job, :top_end
  plurimedia  = job["plurimedia"]

  # Extract stamps
  transfer_web_done     = get_stamp job, :transfer_web, :done
  transfer_web_started  = get_stamp job, :transfer_web, :started
  transfer_web_complete = get_stamp job, :transfer_web, :complete

  transfo_web_done      = get_stamp job, :transfo_web, :done
  transfo_web_started   = get_stamp job, :transfo_web, :started
  transfo_web_video     = get_stamp job, :transfo_web, :video
  transfo_web_transfer  = get_stamp job, :transfo_web, :transfer

  ohe_decoupe_done      = get_stamp job, :ohe_decoupe, :done
  ohe_decoupe_started   = get_stamp job, :ohe_decoupe, :started
  ohe_decoupe_complete  = get_stamp job, :ohe_decoupe, :complete

  activation_web_done   = get_stamp job, :activation_web, :done

  # Computations
  duration = get_distance top_start, top_end

  # transfo_web_times
  out << job["job_id"]
  out << plurimedia
  out << top_start
  out << top_end
  out << duration

  out << get_distance(ohe_decoupe_done, ohe_decoupe_started)
  out << get_distance(ohe_decoupe_started, ohe_decoupe_complete)
  out << get_percent(ohe_decoupe_started, ohe_decoupe_complete, duration)

  out << get_distance(transfer_web_done, transfer_web_started)
  out << get_distance(transfer_web_started, transfer_web_complete)
  out << get_percent(transfer_web_started, transfer_web_complete, duration)

  out << get_distance(transfo_web_done, transfo_web_started)
  out << get_distance(transfo_web_started, transfo_web_transfer)
  out << get_percent(transfo_web_started, transfo_web_transfer, duration)

  out << get_distance(transfo_web_transfer, activation_web_done)

  out
end

# Read jobs and prepare table
puts "Fetching from #{API_URL}"
jobs = fetch

# Build jobs list
puts "Building jobs list"
table2 = Terminal::Table.new
table2.title = "Jobs list"
table2.headings = [
  :job_id, :plurimedia, :top_start, :top_end, :duration,
  "ohe d>s", "ohe s>c", "ohe %",
  "trw d>s", "trw s>e", "trw %",
  "to_web d>s", "to_web s>t", "to_web video%",
  "to_web.t > act_web.d",
  ]
table2.align_column(5, :right)
table2.rows = jobs.collect{ |job| table_jobs_row job }

# Build plurimedia list
puts "Building plurimedia list"
table1 = Terminal::Table.new
table1.title = "Plurimedia list"
table1.headings = [
  :plurimedia, :count,
  "job IDs", "min created_at", "max created_at",
  "min_ohe_decoupe_done", "top_end_of_job_with_latest_created_at", "<< delta",
  "max_activation_web_done", "min_ohe_decoupe_done>max_activation_web_done"
  ]
table1.align_column(5, :right)
table1.rows = build_plurimedia jobs

# Display output
puts
puts table1
puts
puts table2
puts












