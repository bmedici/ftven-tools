#!/usr/bin/env ruby
require 'terminal-table'
require 'rest_client'
require 'bunny'
require 'yaml'
require 'json'
require 'hashie'
require 'securerandom'

PROXY_IDENT = "proxy"
QUEUE_HOST = `hostname`.to_s.chomp
SEPARATOR  = "="*160


# Load config file
def read_config
  config_name = ARGV[0]
  abort "USAGE: #{$0} profile" if config_name.to_s.empty?

  config_file = File.join(File.dirname(File.expand_path(__FILE__)), "#{config_name}.yml")
  abort "ABORT: missgin config file [#{config_file}]" unless File.exist? config_file

  config = YAML::load_file(config_file)
  abort "ABORT: invalid config file [#{config_file}]" unless config.is_a? Hash

  # Dump configuration
  Hashie.symbolize_keys! config
  puts config.to_yaml

  return config
end


# Start connexion to RabbitMQ
def connect busconf
  abort "ABORT: bus host/port not found" unless busconf.is_a? Hash
  begin
    puts "connecting to #{busconf[:host]} port #{busconf[:port]}"
    conn = Bunny.new host: (busconf[:host].to_s || "localhost").to_s,
      port: (busconf[:port] || 5672).to_i,
      user: (busconf[:user] || "guest").to_s,
      pass: (busconf[:pass] || "guest").to_s,
      heartbeat: :server
    conn.start
  rescue Bunny::TCPConnectionFailedForAllHosts, Bunny::AuthenticationFailureError, AMQ::Protocol::EmptyResponseError  => e
    abort "ABORT: error connecting to RabbitMQ (#{e.class})"
  rescue Exception => e
    abort "ABORT: unknow connection error (#{e.inspect})"
  end

  return conn
end


# Log output
def header rule, sign, topic, route
  puts
  puts SEPARATOR
  puts sprintf "%s | %-20s %1s %-10s | %s",
    DateTime.now.iso8601, rule, sign, topic, route
  puts SEPARATOR
end


def extract ctype, payload, fields = []
  # Force encoding (pftop...)
  utf8payload = payload.force_encoding('UTF-8')

  # Parse payload if content-type provided
  case ctype
    when "application/json"
      # if fields = rule[:payload_extract]
      #   data = payload_extract(payload, fields)
      #   data_source = "extract #{fields.inspect} #{data.keys.count}k"
      return JSON.parse utf8payload

    when "text/plain"
      return utf8payload.to_s

    else
      return utf8payload

  end

  # Handle body parse errors
  rescue Encoding::UndefinedConversionError => e
    puts "\t JSON PARSE ERROR: #{e.inspect}"
    return {}

end


def payload_extract payload, fields = []
    new_payload = payload.force_encoding('UTF-8')
    parsed = JSON.parse new_payload

  rescue Encoding::UndefinedConversionError => e
    puts "\t JSON PARSE ERROR: #{e.inspect}"
    return {}

  else
    return parsed
end

def propagate url, body
  # Nothing more to do if no relay
  return if url.nil? || url.empty?

  # Push message to URL
  puts "> POST #{url}"
  response = RestClient.post url.to_s, body, :content_type => :json
  puts "< #{response.body}"

  rescue Exception => e
    puts "> FAILED: #{e.message}"

end

def handle_message rule_name, rule, delivery_info, metadata, payload
  # Prepare data
  msg_topic = delivery_info.exchange
  msg_rkey = delivery_info.routing_key.force_encoding('UTF-8')
  msg_headers = metadata.headers || {}

  # Extract fields
  data = extract metadata.content_type, payload, rule

  # Announce match
  header rule_name, "<", msg_topic, msg_rkey

  # Build notification payload
  body = {
    # received: msg_topic,
    exchange: msg_topic,
    route: msg_rkey,
    #headers: msg_headers,
    sent_at: msg_headers['sent_at'],
    sent_by: msg_headers['sent_by'],
    data: data,
    }
  pretty_body = JSON.pretty_generate(body)

  # Dump body data
  puts "RULE: #{rule.inspect}"
  puts "APP-ID: #{metadata.app_id}"
  puts "CONTENT-TYPE: #{metadata.content_type}"
  puts pretty_body

  # Propagate data if needed
  propagate rule[:relay], pretty_body
end

def topic channel, name
  @topics ||= {}
  @topics[name] ||= channel.topic(name, durable: true, persistent: true)
end

def shout exchange, keys, body = {}
  # Add timestamp
  headers = {
    sent_at: DateTime.now.iso8601,
    sent_by: PROXY_IDENT
    }

  # Prepare key and data
  routing_key = keys.unshift(PROXY_IDENT).join('.')
  # payload = data

  # Announce shout
  header "SHOUT", ">", PROXY_IDENT, routing_key
  puts JSON.pretty_generate(body) unless body.empty?

  # Publish
  exchange.publish(body.to_json,
    routing_key: routing_key,
    headers: headers,
    app_id: "contributor",
    content_type: "application/json",
    )

end


# Load config file
config = read_config

# Start connexion to RabbitMQ and create channel
conn = connect config[:bus]
channel = conn.create_channel
abort "ABORT: config file missing [rules] section" unless config[:rules].is_a? Hash


# Init ASCII table
config_table = Terminal::Table.new
config_table.title = "Message propagation rules"
config_table.headings = ["queue binding", "topic", "route", "relay", "title"]
config_table.align_column(5, :right)


# Bind every topic
config[:rules].each do |rule_name, rule|
  # Extract information
  catch_topic = rule[:topic].to_s
  catch_routes = rule[:routes].to_s.split(' ')

  if catch_topic.empty? || catch_routes.empty?
    abort "rule [#{rule_name}] is invalid: missing topic / routes"
  end

  # Build / attach to queue
  rule_queue_name = "#{PROXY_IDENT}-#{QUEUE_HOST}-#{rule_name}"

  begin
    # Bind to this topic if not already done
    listen_exchange = topic(channel, catch_topic)

    # Pour this into a queue
    queue = channel.queue(rule_queue_name, auto_delete: false, durable: true)

    # Bind to these events on each route
    catch_routes.each do |route|
      # Bind with this routing key
      queue.bind listen_exchange, routing_key: route
      puts "BIND \t[#{rule_queue_name}] to [#{catch_topic}] / [#{route}]"

      # Add row to table
      config_table.add_row [rule_queue_name, catch_topic, route, rule[:relay].to_s, rule[:title].to_s ]
    end

  rescue Bunny::PreconditionFailed => e
    conn.create_channel.queue_delete(rule_queue_name)
    # puts e.backtrace.inspect
    abort "ABORT: cannot bind [#{catch_topic}] code(#{e.channel_close.reply_code}) message(#{e.channel_close.reply_text})"

  rescue Exception => e
    abort "ABORT: unhandled (#{e.inspect})"

  ensure
  end


  # Subscribe
  queue.subscribe(block: false) do |delivery_info, metadata, payload|
    handle_message rule_name, rule, delivery_info, metadata, payload
  end
end


# Display config and susbcribe to queue
puts config_table


# Prepare shout config
shout_config = config[:shout]
shout_exchange = nil
shout_keys = []

if shout_config.is_a? Hash
  shout_exchange = topic(channel, shout_config[:topic])
  shout_keys = shout_config[:keys] if shout_config[:keys].is_a? Array
end

# Endless loop with shout config

begin
  loop do
    if shout_exchange
      random_string = SecureRandom.hex
      random_key = shout_keys.sample || "random"
      shout shout_exchange, [:ping, random_key, random_string], {}
    end
    sleep 1
  end
rescue AMQ::Protocol::EmptyResponseError => e
  abort "ERROR: AMQ::Protocol::EmptyResponseError (#{e.inspect})"
rescue Bunny::TCPConnectionFailedForAllHosts => e
  abort "ERROR: cannot connect to RabbitMQ hosts (#{e.inspect})"
rescue Bunny::ChannelAlreadyClosed => e
  abort "ERROR: channel unexpectedly closed (#{e.inspect})"
  # sleep 1
  # retry
rescue Bunny::PreconditionFailed => e
  abort "ERROR: precondition failed (#{e.inspect})"
rescue Interrupt => e
  channel.close
  conn.close
  abort "QUITTING"
end

puts
puts "ENDED"
