#!/usr/bin/env ruby

require 'spreadsheet'
require 'terminal-table'
require 'mail'
require 'tempfile'

require_relative 'replay_helpers'

API_URL       = "http://webservices.francetelevisions.fr/replay/jobs?limit=100&sort=r.jobId&direction=desc"
SEND_SUBJECT  = "Rapport KPIs"
SEND_FROM     = "Bruno FTVEN <bruno.medici.ext@francetv.fr>"
SEND_TO       = "Bruno <bruno@brunom.net>"
# SEND_SMTP     = "smtp.gmail.com"

def build_plurimedia jobs
  jobs.group_by{ |job| job["plurimedia"] }.collect do |plurimedia, jobs|
    prepare_plurimedia_row plurimedia, jobs
  end
end

def prepare_plurimedia_row plurimedia, jobs
  out = []

  created_at_s = jobs.collect{ |job| get_time job, :started_at }

  ohe_decoupe_done_s = jobs.collect{ |job| get_stamp job, :ohe_decoupe, :done }.reject{ |t| t.nil? }
  min_ohe_decoupe_done = ohe_decoupe_done_s.min

  job_with_latest_created_at = jobs.sort_by{ |job| get_time job, :created_at}.last
  top_end_of_job_with_latest_created_at = get_time job_with_latest_created_at, :top_end

  activation_web_done_s = jobs.collect{ |job| get_stamp job, :activation_web, :done }.reject{ |t| t.nil? }
  max_activation_web_done = activation_web_done_s.max

  out << plurimedia
  out << jobs.size
  out << jobs.collect{ |job| job["job_id"] }.join(' ')

  created_at_min = created_at_s.min
  created_at_max = created_at_s.max
  out << ftime(created_at_min)
  out << if (created_at_s.size == 1)
    "-"
  elsif created_at_max == created_at_min
    "="
  else
    ftime(created_at_max)
  end

  out << ftime(min_ohe_decoupe_done)
  out << ftime(top_end_of_job_with_latest_created_at)
  out << get_distance(top_end_of_job_with_latest_created_at, min_ohe_decoupe_done)

  out << ftime(max_activation_web_done)
  out << get_distance(min_ohe_decoupe_done, max_activation_web_done)

  out
end

def build_jobs jobs
  jobs.collect do |job|
    prepare_jobs_row job
  end
end

def prepare_jobs_row job
  out = []

  # Extract attributes
  top_start   = get_time job, :top_start
  top_end     = get_time job, :top_end
  plurimedia  = job["plurimedia"]

  # Extract stamps: OHE
  ohe_decoupe_done      = get_stamp job, :ohe_decoupe, :done
  ohe_decoupe_started   = get_stamp job, :ohe_decoupe, :started
  ohe_decoupe_complete  = get_stamp job, :ohe_decoupe, :complete

  # Extract stamps: Arkena
  transfer_web_done     = get_stamp job, :transfer_web, :done
  transfer_web_started  = get_stamp job, :transfer_web, :started
  transfer_web_complete = get_stamp job, :transfer_web, :complete

  transfo_web_done      = get_stamp job, :transfo_web, :done
  transfo_web_started   = get_stamp job, :transfo_web, :started
  transfo_web_video     = get_stamp job, :transfo_web, :video
  transfo_web_transfer  = get_stamp job, :transfo_web, :transfer

  # Extract stamps: Pixagility
  transfer_fai_done     = get_stamp job, :transfer_fai, :done
  transfer_fai_started  = get_stamp job, :transfer_fai, :started
  transfer_fai_complete = get_stamp job, :transfer_fai, :complete

  transfo_fai_done      = get_stamp job, :transfo_fai, :done
  transfo_fai_started   = get_stamp job, :transfo_fai, :started
  transfo_fai_video     = get_stamp job, :transfo_fai, :video
  transfo_fai_complete  = get_stamp job, :transfo_fai, :complete

  # Extract stamps: other
  activation_web_done   = get_stamp job, :activation_web, :done


  # Computations
  duration = get_distance top_start, top_end

  # transfo_web_times
  out << job["job_id"]
  out << plurimedia
  out << top_start
  out << top_end
  out << duration

  # Decoupe
  out << get_distance(ohe_decoupe_done, ohe_decoupe_started)
  out << get_distance(ohe_decoupe_started, ohe_decoupe_complete)
  out << get_percent( ohe_decoupe_started, ohe_decoupe_complete, duration)

  # Arkena
  out << get_distance(transfer_web_done, transfer_web_started)
  out << get_distance(transfer_web_started, transfer_web_complete)
  out << get_percent( transfer_web_started, transfer_web_complete, duration)

  out << get_distance(transfo_web_done, transfo_web_started)
  out << get_distance(transfo_web_started, transfo_web_video)
  out << get_percent( transfo_web_started, transfo_web_video, duration)

  out << get_distance(transfo_web_video,  activation_web_done)

  # Pixagility
  out << get_distance(transfer_fai_done, transfer_fai_started)
  out << get_distance(transfer_fai_started, transfer_fai_complete)
  out << get_percent( transfer_fai_started, transfer_fai_complete, duration)

  out << get_distance(transfo_fai_done, transfo_fai_started)
  out << get_distance(transfo_fai_started, transfo_fai_video)
  out << get_percent( transfo_fai_started, transfo_fai_video, duration)

  out
end


####################################################################################################
## Get data
####################################################################################################

# Fetch jobs
puts "FETCHING \t#{API_URL}"
jobs = fetch_jobs

# Create a new Workbook
spreadsheet = Spreadsheet::Workbook.new


####################################################################################################
## Table: jobs list
####################################################################################################
title = "Jobs list"
puts "BUILDING \t#{title}"

# Common
rows = build_jobs jobs
headers = [
  "job_id", "plurimedia", "top_start", "top_end", "duration",

  # Decoupe
  "ohe.do>st", "ohe.st>co", "ohe %",

  # Arkena
  "tx_web.do>st", "tx_web.st>co", "tx_web %",
  "to_web.do>st", "to_web.st>vi", "st>vi %",
  "to_web.vi>act_web.do",

  # Pixagility
  "tx_fai.do>st", "tx_fai.st>en", "tx_fai %",
  "to_fai.do>st", "to_fai.st>vi", "st>vi %"
  ]

# Build ASCII table
table2 = Terminal::Table.new
table2.title = title
table2.headings = headers
table2.align_column(5, :right)
table2.rows = rows

# Build Excel sheet
sheet2 = spreadsheet.create_worksheet :name => title
sheet2.row(0).replace headers
idx = 2
rows.each do |row|
  sheet2.row(idx).replace row
  idx += 1
end



####################################################################################################
## Table: plurimedia list
####################################################################################################
title = "Plurimedia list"
puts "BUILDING \t#{title}"

# Common
rows = build_plurimedia jobs
headers = [
  "plurimedia", "count",
  "job_ids", "min created_at", "max created_at",
  "min_ohe_decoupe_done", "top_end_of_job_with_latest_created_at", "<< delta",
  "max_activation_web_done", "min_ohe_decoupe_done>max_activation_web_done"
  ]

# Build ASCII table
table1 = Terminal::Table.new
table1.title = title
table1.headings = headers
table1.align_column(5, :right)
table1.rows = rows

# Build Excel sheet
sheet1 = spreadsheet.create_worksheet :name => title
sheet1.row(0).replace headers
idx = 2
rows.each do |row|
  sheet1.row(idx).replace row
  idx += 1
end


####################################################################################################
## Output: screen
####################################################################################################

# Display output
ascii = [table1, "\n\n\n\n", table2, "\n\n\n\n"]
#puts ascii


####################################################################################################
## Output: email
####################################################################################################
puts "EMAILING \t[#{SEND_FROM}] > [#{SEND_TO}]"

# Compile spreadsheet
excel_file = Tempfile.new('replay_kpi')
spreadsheet.write(excel_file.path)

# Send email
Mail.deliver do
  delivery_method :sendmail
  from      SEND_FROM
  to        SEND_TO
  subject   "#{SEND_SUBJECT} #{ftime Time.now}"
  body      ascii
  add_file  filename: 'replay_kpis.xls', content: excel_file.read
end




