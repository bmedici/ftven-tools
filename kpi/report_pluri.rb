class ReportPluri < ReportTable

  def initialize jobs, title
    @headers = [
      "plurimedia", "count",
      "job_ids", "< started_at", "started_at >",
      "prem decoupe", "top_end dernier job", "delta",
      "der activ web", "prem decoupe > der activ web"
      ]
    # @headers = [
    #   "plurimedia", "count",
    #   "job_ids", "◁ created_at", "created_at ▷",
    #   "◁ ohe_decoupe_done", "top_end_of_job_with_latest_created_at", "delta",
    #   "activation_web_done ▷", "◁ ohe_decoupe_done -- activation_web_done ▷"
    #   ]

    super
  end

  def build_rows
    @jobs.group_by(&:plurimedia).collect do |plurimedia, jobs|
      build_row plurimedia, jobs
    end
  end

  def build_row plurimedia, jobs
    out = []
    # percent(   job.transfo_fai_started,  job.transfo_fai_video,      duration)

    # Headers
    out << plurimedia
    out << jobs.size
    out << jobs.collect(&:job_id).join(' ')

    # Start times
    started_ats = jobs.collect(&:started_at)
    started_at_min = started_ats.min
    started_at_max = started_ats.max
    out << ftime(started_at_min)
    if (started_ats.size == 1)|| started_at_max == started_at_min
      out << "="
    else
      out << ftime(started_at_max)
    end


    # Première découpe
    ohe_decoupe_done_s = jobs.collect(&:ohe_decoupe_done).reject{ |t| t.nil? }
    min_ohe_decoupe_done = ohe_decoupe_done_s.min
    out << ftime(     min_ohe_decoupe_done)

    # Les tentatives
    last_started_job = jobs.sort_by(&:started_at).last
    top_end_of_last_started_job = last_started_job.top_end

    out << ftime(     top_end_of_last_started_job   )
    out << distance(  top_end_of_last_started_job,    min_ohe_decoupe_done)

    # Activation times
    activation_web_done_s = jobs.collect(&:activation_web_done).reject{ |t| t.nil? }
    max_activation_web_done = activation_web_done_s.max

    out << ftime(     max_activation_web_done       )
    out << distance(  min_ohe_decoupe_done,           max_activation_web_done)


    out
  end

end
